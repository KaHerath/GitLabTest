import org.testng.Assert;
import org.testng.annotations.Test;

public class IntegrationTest extends BaseTest {

    @Test(groups = {"Smoke"})
    public void testCase001() {
        getDriver().navigate().to("http://www.google.com");
        Assert.assertEquals(getDriver().getTitle(), "Google");
    }

    @Test(groups = {"Smoke"})
    public void testCase002() {
        getDriver().navigate().to("http://www.google.com");
        Assert.assertEquals(getDriver().getTitle(), "Google");
    }

    @Test(groups = {"Smoke"})
    public void testCase003() {
        getDriver().navigate().to("http://www.google.com");
        Assert.assertEquals(getDriver().getTitle(), "Google");
    }

    @Test(groups = {"Smoke"})
    public void testCase004() {
        getDriver().navigate().to("http://www.google.com");
        Assert.assertEquals(getDriver().getTitle(), "Google");
    }
}
